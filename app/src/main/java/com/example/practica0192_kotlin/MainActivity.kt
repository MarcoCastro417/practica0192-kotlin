package com.example.practica0192_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var btnPulsar: Button
    private lateinit var txtNombre: EditText
    private lateinit var lblSaludar: TextView
    private lateinit var btnLimpiar: Button
    private lateinit var btnCerrar: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Relacionar los objetos
        btnPulsar = findViewById(R.id.btnSaludar)
        txtNombre = findViewById(R.id.txtNombre)
        lblSaludar = findViewById(R.id.lblSaludo)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnCerrar = findViewById(R.id.btnCerrar)
        // Codificar el evento click del botón
        btnPulsar.setOnClickListener {
            // Validar
            if (txtNombre.text.toString().matches("^$".toRegex())) {
                Toast.makeText(this, "Faltó capturar información", Toast.LENGTH_SHORT).show()
            } else {
                val str = "Hola ${txtNombre.text.toString()}, ¿cómo estás?"
                // Toast.makeText(this, str, Toast.LENGTH_SHORT).show()
                lblSaludar.text = str
            }
        }
        btnLimpiar.setOnClickListener {
            lblSaludar.text = ":: ::"
            txtNombre.setText("")
        }
        btnCerrar.setOnClickListener {
            finish()
        }
    }
}